/**
 * Recives a string and returns all it's permutations as an array of strings
 * @param {string} string
 * @returns {string[]}
 */
export const permuteString = (string = "") => {
  if (!string || typeof string !== "string") {
    return [];
  }
  const result = [];
  const maxSize = 4.2e7;
  for (const item of permute(sanitizeStringToArray(string))) {
    result.push(item.join());
    if (result.length === maxSize) {
      console.warn("result chunked to avoid collapsing v8", maxSize);
      logMem(result.length); // Usefull for debugging but will slow down execution by hours
      return result;
    }
  }
  return result;
};

/**
 * It's better to just return a generator than a plain array since the engine can't take the full alphabet
 * @param {string} string
 */
export function* betterPermutations(string = "") {
  for (const item of permute(sanitizeStringToArray(string))) {
    yield item;
  }
}

function logMem(arraySize = 0) {
  const memory = process.memoryUsage().heapUsed / 1024 / 1024;
  console.log(
    `The script uses approximately ${
      Math.round(memory * 100) / 100
    } on size ${arraySize}`
  );
}

/**
 * We don't care for duplicate letters so we create a set to filter them before the generator
 *
 * There was no specification to limit this to only ASCII letters, but here it is
 * @param {string} string
 */
const sanitizeStringToArray = (string) =>
  Array.from(new Set(string.normalize("NFKD").toLowerCase().match(/\w/gi)));

/**
 * Permutes over an array of letters and yields an array of letters
 *
 * @param {string[]} stringArray
 * @param {number} length
 * @returns {Generator<string[]>}
 */
function* permute(stringArray, length = stringArray.length) {
  if (length <= 1) {
    // If we already shifted all the possible letters to shift, yield
    yield stringArray.slice();
  } else {
    const index = length - 1;
    // Loop through the lenght of the array or the passed down cursor
    for (let cursor = 0; cursor < length; cursor++) {
      // Recurse on the same operation to shift from left to right
      yield* permute(stringArray, index);
      // Get the index to shift position by pairs depending on the cursor
      const position = length % 2 ? 0 : cursor;
      // Shift the position of the elements
      const right = stringArray[index];
      const left = stringArray[position];
      stringArray.splice(index, 1, left);
      stringArray.splice(position, 1, right);
    }
  }
}

// console.log(
//   permuteString(`
//     As some day it may happen that a victim must be found
//     I've got a little list -- I've got a little list
//     Of society offenders who might well be underground
//     And who never would be missed -- who never would be missed.
//                     -- Koko, "The Mikado"
//     `).length
// );
