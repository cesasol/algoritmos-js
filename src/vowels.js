/**
 * The simplest solution for vowels.
 *
 * @param {string} string
 * @returns {number}
 */
export const countVowelsRegex = (string = "") => {
  return typeof string === "string"
    ? string.match(/[aeiou]/gi)?.length || 0
    : 0;
};

/**
 * Counts the vowel with reduce iteration, is the most consise but a for of would be faster.
 *
 * @param {string} string
 */
export const countVowels = (string = "") => {
  if (typeof string !== "string") {
    return 0;
  }
  const vowels = "aeiou";
  return string
    .toLowerCase()
    .split("")
    .reduce((count, letter) => {
      if (vowels.includes(letter)) {
        count++;
      }
      return count;
    }, 0);
};
