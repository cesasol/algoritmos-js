import { compressString } from "./stringCompression";

describe("StringCompression()", () => {
  test("No op with single letter string", () => {
    expect(compressString("a")).toBe("a");
  });
  test("Returns the same string if the result will be larger than the input", () => {
    expect(compressString("abcdefghijklmn")).toBe("abcdefghijklmn");
  });
  test("Compress simple occurence of a and b", () => {
    expect(compressString("aaabbbbaaaabbbbaba")).toBe("a9b9");
  });
  test("Compress and sort an entire paragraph", () => {
    expect(
      compressString(`A young honeymoon couple were touring southern Florida and happened
to stop at one of the rattlesnake farms along the road.  After seeing the
sights, they engaged in small talk with the man that handled the snakes.
"Gosh!" exclaimed the new bride.  "You certainly have a dangerous job.
Don't you ever get bitten by the snakes?"
        "Yes, upon rare occasions," answered the handler.
        "Well," she continued, "just what do you do when you're bitten by
a snake?"
        "I always carry a razor-sharp knife in my pocket, and as soon as I
am bitten, I make deep criss-cross marks across the fang entry and then
suck the poison from the wound."
        "What, uh... what would happen if you were to accidentally *sit* on
a rattler?" persisted the woman.
        "Ma'am," answered the snake handler, "that will be the day I learn
who my real friends are."
`)
    ).toBe(
      "a64b8c14d27e80f9g11h37i28j2k11l23m14n51o44p12r38s41t53u17v2w17x1y19z1"
    );
  });
});
