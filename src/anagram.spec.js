import { sortAnagrams } from "./anagram";

describe("Anagram()", () => {
  test("Expect two anagrams to be close to each other", () => {
    expect(
      sortAnagrams([
        "evil",
        "not evil",
        "some filler",
        "this is not an anagram",
        "vile",
      ])
    ).toStrictEqual([
      "evil",
      "vile",
      "not evil",
      "some filler",
      "this is not an anagram",
    ]);
  });
  test("Expect half of wikipedia page anagrams to be close to each other", () => {
    expect(
      sortAnagrams([
        "New York Times",
        "Anagrams as a commentary on the subject.",
        "monkeys write",
        "They may be For example:",
        "Church of Scientology",
        "may be created",
        "rich-chosen goofy cult",
        "McDonald's restaurants",
        "parody, a criticism",
        "Uncle Sam's standard rot",

        "An anagram synonym",

        "evil",
        "a or satire",
        "vile",
        "may also be a",
        "a gentleman",
        "of . For example:",
        "elegant man",
        "the original word",
        "eleven plus two",
        "something else to put in the midle",
        "twelve plus one",
      ])
    ).toStrictEqual([
      "New York Times",
      "monkeys write",
      "Church of Scientology",
      "rich-chosen goofy cult",
      "McDonald's restaurants",
      "Uncle Sam's standard rot",
      "evil",
      "vile",
      "a gentleman",
      "elegant man",
      "eleven plus two",
      "twelve plus one",
      "Anagrams as a commentary on the subject.",
      "They may be For example:",
      "may be created",
      "parody, a criticism",
      "An anagram synonym",
      "a or satire",
      "may also be a",
      "of . For example:",
      "the original word",
      "something else to put in the midle",
    ]);
  });
});
