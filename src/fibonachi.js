export const fibonachi = () => {
  let count = 0;
  let fibN1 = 0;
  let fibN2 = 1;
  const result = [];
  while (count < 100) {
    result.push(fibN1);
    let nth = fibN1 + fibN2;
    fibN1 = fibN2;
    fibN2 = nth;
    count += 1;
  }
  return result;
};
