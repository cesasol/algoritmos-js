/**
 * Checks if the string is a palindrome
 *
 * @param {string} string
 * @returns {boolean}
 */
export const isPalindrome = (string) => {
  if (!string || typeof string !== "string") {
    return false;
  }
  // We want only ASCII letters so convert them if possible
  const normalized = string.normalize("NFKD").toLowerCase().match(/[\w]/gi);
  return normalized
    ? normalized.slice().reverse().join("") === normalized.join("")
    : false;
};
