/**
 * Compress a string to a simple iteration notation, recovery does not matter
 * @param {string} string
 * @returns {string}
 */
export const compressString = (string = "") => {
  if (typeof string !== "string") {
    return "";
  }
  if (string.length <= 1) {
    return string;
  }
  const uniques = new Set(
    string.toLowerCase().match(/[\w+]/gi)?.join("").split("").sort()
  );
  const counted = [];
  for (const letter of uniques) {
    counted.push(
      letter + string.replace(new RegExp(`[^${letter}]`, "gi"), "").length
    );
  }
  const final = counted.join("");
  if (final.length >= string.length) {
    return string;
  }
  return final;
};
