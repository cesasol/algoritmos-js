import { isPalindrome } from "./palindrome";

describe("Palindrome()", () => {
  test.each`
    word
    ${"Radar"} | ${"Repaper"} | ${"Rotator"} | ${"anna"}
  `("Test that the palindrome $word returns true", ({ word }) => {
    expect(isPalindrome(word)).toBe(true);
  });
  test("the longest single word english palindrome", () => {
    expect(isPalindrome("tattarrattat")).toBe(true);
  });
  test("the longest single word palindrome ever", () => {
    expect(isPalindrome("saippuakivikauppias")).toBe(true);
  });
  test("The a spanish multiple word palindorme", () => {
    expect(isPalindrome("dábale arroz a la zorra el abad.")).toBe(true);
  });
  test("Multi word palindorme with punctuation", () => {
    expect(
      isPalindrome(
        "Are we not pure? “No sir!” Panama’s moody Noriega brags. “It is garbage!” Irony dooms a man; a prisoner up to new era."
      )
    ).toBe(true);
  });
  test("single words non palindorme", () => {
    expect(isPalindrome("not")).toBe(false);
    expect(isPalindrome("maybe")).toBe(false);
    expect(isPalindrome("yes")).toBe(false);
  });
  test("Long paragraph non palindrome", () => {
    expect(
      isPalindrome(
        "Ham kielbasa ea ham hock reprehenderit aute capicola consectetur irure short loin velit hamburger esse beef ad. Sausage sint short ribs cow nostrud mollit ex ut shank shankle chuck incididunt cupidatat. Salami biltong chislic ut qui non short loin velit dolor flank pork chop meatloaf dolore. Shoulder fatback boudin ullamco, consectetur irure elit non meatball meatloaf cupidatat corned beef ball tip nostrud. Alcatra exercitation qui, veniam pariatur excepteur tail. Buffalo in ut sunt, esse nulla pig shankle. Esse corned beef voluptate cillum ball tip."
      )
    ).toBe(false);
  });
});
