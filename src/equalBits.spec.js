import { hasSameOnes } from "./equalBits";

describe("equalBits()", () => {
  test("52 has 3 ones on its bits, also 50 and 56", () => {
    const originalLog = console.log;
    const mockLog = jest.fn();
    console.log = mockLog;
    hasSameOnes(52);
    expect(mockLog).toHaveBeenNthCalledWith(1, 56);
    expect(mockLog).toHaveBeenNthCalledWith(2, 50);
    mockLog.mockReset();
    mockLog.mockRestore();
    console.log = originalLog;
  });
});
