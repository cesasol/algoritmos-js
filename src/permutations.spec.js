import { betterPermutations, permuteString } from "./permutations";
describe("Permutations()", () => {
  test("Single letter permutation should do nothing", () => {
    expect(permuteString("a")).toStrictEqual(["a"]);
  });
  test("Run on a medium sized string", () => {
    expect(permuteString("abcdefghij").length).toBe(3628800);
  });
  // WARN: This test has the potential of killing your pc or just crashing.
  xtest("Full cite permutation expecting it to return an incomplete set", () => {
    expect(
      permuteString(`
    As some day it may happen that a victim must be found
    I've got a little list -- I've got a little list
    Of society offenders who might well be underground
    And who never would be missed -- who never would be missed.
                    -- Koko, "The Mikado"
    `).length
    ).toEqual(4.2e7);
  });
  test("Full alphabet with a generator permutator", () => {
    let count = BigInt(0);
    for (const item of betterPermutations("abcdefghijklmnopqrstuvwxyz")) {
      count++;
    }
    console.log(count);
    expect(count).toBeGreaterThan(4.2e7);
  });
});
