import { countVowels } from "./vowels";

describe("Vowels()", () => {
  test("empty sentence", () => {
    expect(countVowels("")).toBe(0);
  });
  test("Perfect lower case", () => {
    expect(countVowels("aeiou")).toBe(5);
  });
  test("only lower case", () => {
    expect(countVowels("the quick brown fox jumps over the lazy dog")).toBe(11);
  });
  test("Mixed case", () => {
    expect(
      countVowels("My sense of purpose is gone! I have no idea who I AM!")
    ).toBe(19);
  });
  test("no vowels symbols and numbers", () => {
    expect(countVowels("PXRY1-v1.1.0.4")).toBe(0);
  });
  test("email address has some", () => {
    expect(countVowels("199710290036.QAA01818@wall.org")).toBe(4);
  });
});
