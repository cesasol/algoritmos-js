/**
 * Write a method to sort an array of strings so that all the anagrams are next toeach other
 * @param {string[]} arrayOfStrings
 * @returns {string[]}
 */
export const sortAnagrams = (arrayOfStrings = []) => {
  if (!Array.isArray(arrayOfStrings)) {
    return [];
  }
  if (arrayOfStrings.length < 2) {
    return arrayOfStrings;
  }
  /** @type {Map<string, string>} */
  const keysMap = new Map();
  const anagrams = new Set();
  const notAnagrams = new Set();
  arrayOfStrings.forEach((string) => {
    const key = string.toLowerCase().match(/[\w]/g)?.sort().join("") || "";
    if (keysMap.has(key)) {
      anagrams.add(keysMap.get(key));
      anagrams.add(string);
      notAnagrams.delete(keysMap.get(key));
    } else {
      keysMap.set(key, string);
      notAnagrams.add(string);
    }
  });
  return [...anagrams, ...notAnagrams];
};
