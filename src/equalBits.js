/**
 * receives a positive number(integer) as a parameter and then
 * it prints the next smallest and the next largest number that
 * have the same number of 1 bits in their binary representation.
 *
 * @param {number} number
 */
export const hasSameOnes = (number = 0) => {
  if (typeof number !== "number" || !Number.isInteger(number) || number < 1) {
    throw new Error("Param was not a positive integer");
  }
  const bits = number.toString(2);
  const numberOfOnes = bits.replaceAll("0", "").length;
  // Look for next
  let next = null;
  let nextIteration = number + 1;
  // Loop for next number with same number of ones
  while (next === null) {
    const bitsForNext = nextIteration.toString(2);
    if (bitsForNext.replaceAll("0", "").length === numberOfOnes) {
      next = nextIteration;
    }
    nextIteration++;
  }
  // Look for prev
  let prev = null;
  let prevIteration = number;
  // Loop for prev number with same number of ones
  while (prev === null) {
    prevIteration--;
    const bitsForPrev = prevIteration.toString(2);
    if (bitsForPrev.replaceAll("0", "").length === numberOfOnes) {
      prev = prevIteration;
    }
  }
  console.log(next);
  console.log(prev);
};
