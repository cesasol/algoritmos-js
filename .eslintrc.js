/** @type {import('eslint').Linter.Config} */
const eslintConfig = {
  root: true,
  env: {
    es2021: true,
  },
  extends: ["airbnb-base", "prettier/airbnb", "prettier"],
  parser: "@babel/eslint-parser",
  parserOptions: {
    ecmaVersion: 12,
    sourceType: "module",
  },
  env: {
    es6: true,
    node: true,
    jest: true,
  },
  plugins: ["@typescript-eslint", "prettier"],
  rules: {
    "import/prefer-default-export": 0,
    "import/no-default-export": 1,
  },
};
export default eslintConfig;
